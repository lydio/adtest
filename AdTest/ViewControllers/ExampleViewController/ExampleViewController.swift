//
//  ViewController.swift

import UIKit


class ExampleViewController: UIViewController {
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    private var adButtons = [UIButton]()
    private var adLabels = [UILabel]()
    
    private let adService = AdService.shared
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupAdControls()
    }
    
    private func setupAdControls() {
        
        let spacing:CGFloat = 8
        let color = UIColor.black
        let font = UIFont.systemFont(ofSize: 12)
        
        let vStackView = UIStackView(frame: .zero)
        vStackView.axis = .vertical
        vStackView.distribution = .fillEqually
        vStackView.spacing = spacing
        
        var hStackView:UIStackView!
        var options: AdService.Options?
        var providerTitles = [String]()
        var formatTitles = [String]()
        
        for i in 0..<adService.availableOptions.count {
            
            if adService.availableOptions[i].provider != options?.provider {
                options = adService.availableOptions[i]
                hStackView = UIStackView(frame: .zero)
                hStackView.axis = .horizontal
                hStackView.distribution = .fillEqually
                hStackView.spacing = spacing
                vStackView.addArrangedSubview(hStackView)
                providerTitles.append(adService.availableOptions[i].provider.rawValue)
            }
            
            if providerTitles.count == 1 {
                formatTitles.append(adService.availableOptions[i].format.rawValue)
            }
            
            let button = makeAdButton(radius: spacing, color: color, tag: i)
            hStackView.addArrangedSubview(button)
            adButtons.append(button)
        }
        
        let adControlsView = UIView()
        view.addSubview(adControlsView)
        
        adControlsView.translatesAutoresizingMaskIntoConstraints = false
        adControlsView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 3.0/4).isActive = true
        adControlsView.heightAnchor.constraint(equalTo: view.widthAnchor, multiplier: 3.0/4).isActive = true
        
        adControlsView.addSubview(vStackView)
        vStackView.translatesAutoresizingMaskIntoConstraints = false
        vStackView.bottomAnchor.constraint(equalTo: adControlsView.bottomAnchor).isActive = true
        vStackView.rightAnchor.constraint(equalTo: adControlsView.rightAnchor).isActive = true
        vStackView.widthAnchor.constraint(equalTo: adControlsView.widthAnchor, multiplier: 2.0/3).isActive = true
        vStackView.heightAnchor.constraint(equalTo: adControlsView.widthAnchor, multiplier: 2.0/3).isActive = true
        vStackView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        vStackView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        
        let providerStackView = UIStackView(frame: .zero)
        providerStackView.axis = .vertical
        providerStackView.distribution = .fillEqually
        
        for i in 0..<providerTitles.count {
            let label = UILabel()
            label.text = providerTitles[i]
            label.textColor = color
            label.textAlignment = .right
            label.font = font
            providerStackView.addArrangedSubview(label)
        }
        
        adControlsView.addSubview(providerStackView)
        providerStackView.translatesAutoresizingMaskIntoConstraints = false
        providerStackView.topAnchor.constraint(equalTo: vStackView.topAnchor).isActive = true
        providerStackView.rightAnchor.constraint(equalTo: vStackView.leftAnchor, constant: -spacing).isActive = true
        providerStackView.leftAnchor.constraint(equalTo: adControlsView.leftAnchor).isActive = true
        providerStackView.bottomAnchor.constraint(equalTo: adControlsView.bottomAnchor).isActive = true
        
        let formatStackView = UIStackView(frame: .zero)
        formatStackView.axis = .horizontal
        formatStackView.distribution = .fillEqually
        formatStackView.alignment = .bottom
        
        for i in 0..<formatTitles.count {
            let label = UILabel()
            label.text = formatTitles[i]
            label.textColor = color
            label.textAlignment = .left
            label.font = font
            label.layer.anchorPoint = CGPoint(x:0,y:0.5)
            label.transform = CGAffineTransform(rotationAngle: -CGFloat.pi/2)
            formatStackView.addArrangedSubview(label)
        }
        
        adControlsView.addSubview(formatStackView)
        formatStackView.translatesAutoresizingMaskIntoConstraints = false
        formatStackView.topAnchor.constraint(equalTo: adControlsView.topAnchor).isActive = true
        formatStackView.rightAnchor.constraint(equalTo: adControlsView.rightAnchor).isActive = true
        formatStackView.leftAnchor.constraint(equalTo: vStackView.leftAnchor).isActive = true
        formatStackView.bottomAnchor.constraint(equalTo: vStackView.topAnchor, constant: 0).isActive = true
    }
    
    private func makeAdButton(radius:CGFloat, color: UIColor, tag:Int) -> UIButton {
        
        let button = UIButton(type: .custom)
        button.layer.masksToBounds = true
        button.layer.cornerRadius = radius
        button.layer.borderWidth = 1
        button.layer.borderColor = color.cgColor
        button.layer.backgroundColor = UIColor.clear.cgColor
        
        button.tag = tag
        button.addTarget(self, action: #selector(didPressAdButton(_:)), for: .touchUpInside)
        
        return button
    }
    
    @objc private func didPressAdButton(_ sender:UIButton) {
        
        adService.stopCurrentTasks()
        
        for c in adButtons {
            if c != sender, c.isSelected {
                c.isSelected = false
                c.layer.backgroundColor = UIColor.clear.cgColor
            }
        }
    
        if sender.isSelected {
            sender.isSelected = false
            sender.layer.backgroundColor = UIColor.clear.cgColor
        } else {
            sender.isSelected = true
            sender.layer.backgroundColor = sender.layer.borderColor
            
            let options = adService.availableOptions[sender.tag]
            adService.startNewTask(withOptions: options, viewController: self)
        }
    }
    
}


