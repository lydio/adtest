//
//  AppDelegate.swift


import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UINavigationControllerDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        
        (window?.rootViewController as? UINavigationController)?.delegate = self
        
        return true
    }
    
    func navigationControllerSupportedInterfaceOrientations(_ navigationController: UINavigationController) -> UIInterfaceOrientationMask {
        
        return navigationController.topViewController?.supportedInterfaceOrientations ?? UIInterfaceOrientationMask.allButUpsideDown
    }
}

