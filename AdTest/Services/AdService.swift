//
//  AdService.swift
//  AdTest

import UIKit


class AdService: NSObject, AdColonyInterstitialDelegate {

    static let shared = AdService()
    
    enum Provider: String, CaseIterable {
        case ironsourse
        case adcolony
        case kidoz
    }
    
    enum Format: String, CaseIterable {
        case banner
        case interstitional
        case rewarded
    }
    
    private class AdColonyInterstitialTask: NSObject {
        
        let zoneID: String
        private(set) weak var viewController:UIViewController?
        var interstitial: AdColonyInterstitial?
        var closeTime:Int = 0
        
        init(zoneID: String, viewController:UIViewController) {
            
            self.zoneID = zoneID
            self.viewController = viewController
            super.init()
        }
    }
    
    typealias Options = (provider: AdService.Provider, format: AdService.Format)
    let availableOptions:[Options]
    
    var closeSecondsMax:Int = 30 {
        didSet {
            restartUpdateTimer()
        }
    }
    private var updateTimer:Timer?
    
    private var adColonyData:(
        zones: [AdColonyZone],
        interstitialTasks : [AdColonyInterstitialTask],
        timer:Timer?
    ) = {
         return ([], [], nil)
    }()
    
    override init() {
        
        let providers = Provider.allCases
        let formats = Format.allCases
        
        var options = [Options]()
        
        for p in 0..<providers.count {
            for f in 0..<formats.count {
                options.append((providers[p], formats[f]))
            }
        }
        
        availableOptions = options
        
        super.init()
        
        let url = Bundle.main.url(forResource: "AdServiceConfig", withExtension: "plist")!
        let config = NSDictionary(contentsOf: url)!
        
        AdColony.configure(
            withAppID: config.value(withPath: "AdColony.AppID"),
            zoneIDs: config.value(withPath: "AdColony.ZoneIDs"),
            options: nil
        ) { [unowned self] (zones) in
            self.adColonyData.zones.append(contentsOf: zones)
        }
        
        restartUpdateTimer()
    }
    
    func startNewTask(withOptions options: Options, viewController:UIViewController) {
        
        switch options.provider {
            case .adcolony:
                switch options.format {
                    case .interstitional:
                        startAdColonyInterstitial(viewController: viewController)
                    break
                    case .rewarded:
                    break
                    case .banner:
                    break
                }
                break
            case .ironsourse:
                switch options.format {
                    case .interstitional:
                    break
                    case .rewarded:
                    break
                    case .banner:
                    break
                }
                break
            case .kidoz:
                switch options.format {
                    case .interstitional:
                    break
                    case .rewarded:
                    break
                    case .banner:
                    break
                }
                break
        }
    }
    
    func stopCurrentTasks() {
        
        adColonyData.interstitialTasks.removeAll()
    }
    
    func adColonyInterstitialDidLoad(_ interstitial: AdColonyInterstitial) {
        
        for t in adColonyData.interstitialTasks {
            
            if t.zoneID == interstitial.zoneID, let vc = t.viewController, t.closeTime == 0 {
                if t.interstitial == nil || t.interstitial!.expired {
                    t.interstitial = interstitial
                    interstitial.show(withPresenting: vc)
                    break
                }
            }
        }
    }
    
    func adColonyInterstitialDidFail(toLoad error: AdColonyAdRequestError) {
        print("Interstitial request failed with error: \(error.localizedDescription) and suggestion: \(error.localizedRecoverySuggestion!)")
    }
    
    func adColonyInterstitialDidClose(_ interstitial: AdColonyInterstitial) {
        
        for t in adColonyData.interstitialTasks {
            if t.interstitial == interstitial, t.closeTime == 0 {
                t.closeTime = Int(CACurrentMediaTime())
                break
            }
        }
    }
    
    func adColonyInterstitialExpired(_ interstitial: AdColonyInterstitial) {
        
        let viewControllers = adColonyData.interstitialTasks.compactMap { $0.viewController }
        for vc in viewControllers {
            startAdColonyInterstitial(viewController: vc)
        }
    }
    
    func startAdColonyInterstitial(viewController:UIViewController?) {
        
        guard
            let zone = adColonyData.zones.first/*(where: { $0.type == .interstitial })*/,
            let vc = viewController
        else {
            return
        }
        
        for t in adColonyData.interstitialTasks {
            if t.zoneID == zone.identifier && t.viewController == vc {
                return
            }
        }
        
        let task = AdColonyInterstitialTask(zoneID: zone.identifier, viewController: vc)
        adColonyData.interstitialTasks.append(task)
        
        AdColony.requestInterstitial(inZone: zone.identifier, options: nil, andDelegate: self)
    }
    
    private func restartUpdateTimer() {
        
        updateTimer?.invalidate()
        
        updateTimer = Timer.scheduledTimer(withTimeInterval: 5, repeats: true, block: { [unowned self] (_) in
            
            for t in self.adColonyData.interstitialTasks {
                
                if let vc = t.viewController, t.closeTime > 0 {
                    if Int(CACurrentMediaTime()) - t.closeTime > self.closeSecondsMax {
                        
                        self.adColonyData.interstitialTasks = self.adColonyData.interstitialTasks.filter { $0 != t }
                        self.startAdColonyInterstitial(viewController: vc)
                    }
                }
            }
        })
    }
}

private extension NSDictionary {
    
    func value<T>(withPath path:String) -> T {
        
        let components = path.components(separatedBy: ".")
        var dict: NSDictionary = self
        
        for i in 0..<components.count {
            
            let value = dict[components[i]]
            
            if value is NSDictionary {
                dict = value as! NSDictionary
            } else if i == components.count - 1, value is T {
                return value as! T
            }
        }
        fatalError("value not found")
    }
}
